import psutil
import timeit
import csv
from threading import Timer
from datetime import datetime


def virtual_keyboard_in_use():
    for proc in psutil.process_iter():
        try:
            if "keyboard" in proc.name().lower():
                return True
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    return False

# Assume virtual keyboard uses 0.5 watts
virtual_keyboard_power_usage = 0.5  
virtual_keyboard_usage_time = 0  # Initialize virtual keyboard usage time to 0


def power_consumption():
    global virtual_keyboard_usage_time
    return virtual_keyboard_power_usage * (virtual_keyboard_usage_time / 3600)  # power consumption in watt-hours

def battery_monitor():
    global virtual_keyboard_usage_time

    # Get the current battery level (0-100)
    battery = psutil.sensors_battery()
    plugged = battery.power_plugged
    percent = battery.percent
    if plugged==False:
        remaining_time = (battery.secsleft / 60) - (virtual_keyboard_usage_time / 60)

        print(f'Battery level: {percent}%, Remaining usage time: {remaining_time} minutes.')
    else:
        print(f'Battery level: {percent}%, charging')
    Timer(60, battery_monitor).start()

def virtual_keyboard_monitor():
    global virtual_keyboard_usage_time
    # Code to check if virtual keyboard is in use
    if virtual_keyboard_in_use():
        virtual_keyboard_usage_time += 1  # increment virtual keyboard usage time by 1 second
        power_consumption = virtual_keyboard_power_usage * (virtual_keyboard_usage_time / 3600)
        with open('virtual_keyboard_consumption.csv', 'a', newline='') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow([datetime.now().strftime("%Y-%m-%d %H:%M:%S"), power_consumption])
        print("power_consumption :",power_consumption)
    Timer(1, virtual_keyboard_monitor).start()

virtual_keyboard_monitor()
battery_monitor()
