import psutil
import time
import platform

def get_energy():
    cpu_power = 0
    mem_power = 0
    if platform.system() == 'Linux':
        with open('/sys/devices/virtual/powercap/intel-rapl/intel-rapl:0/energy_uj', 'r') as f:
            cpu_energy_before = int(f.read().strip())
        with open('/sys/devices/virtual/powercap/intel-rapl/intel-rapl:0/intel-rapl:0:1/energy_uj', 'r') as f:
            mem_energy_before = int(f.read().strip())
        time.sleep(1)
        with open('/sys/devices/virtual/powercap/intel-rapl/intel-rapl:0/energy_uj', 'r') as f:
            cpu_energy_after = int(f.read().strip())
        with open('/sys/devices/virtual/powercap/intel-rapl/intel-rapl:0/intel-rapl:0:1/energy_uj', 'r') as f:
            mem_energy_after = int(f.read().strip())
        cpu_energy_diff = cpu_energy_after - cpu_energy_before
        mem_energy_diff = mem_energy_after - mem_energy_before
        cpu_power = cpu_energy_diff / 1000000 # Convert uJ to mJ
        mem_power = mem_energy_diff / 1000000 # Convert uJ to mJ
    elif platform.system() == 'Windows':
        cpu_percent_before = psutil.cpu_percent()
        mem_percent_before = psutil.virtual_memory().percent
        time.sleep(1)
        cpu_percent_after = psutil.cpu_percent()
        mem_percent_after = psutil.virtual_memory().percent
        cpu_percent_diff = cpu_percent_after - cpu_percent_before
        mem_percent_diff = mem_percent_after - mem_percent_before
        cpu_power = cpu_percent_diff / 100 * psutil.cpu_count() * psutil.cpu_freq().current / 1000
        mem_power = mem_percent_diff / 100 * psutil.virtual_memory().total / 1024 / 1024 / 1024 * 3.3
    return (cpu_power, mem_power)

print('Press Enter to start measuring energy consumption...')
input()
cpu_power_before, mem_power_before = get_energy()
print('Please write "math formula, word text, excel" using the virtual keyboard...')
while True:
    try:
        cpu_power, mem_power = get_energy()
        cpu_power_diff = cpu_power - cpu_power_before
        mem_power_diff = mem_power - mem_power_before
        print(f'CPU energy consumption: {cpu_power_diff:.2f} mJ')
        print(f'Memory energy consumption: {mem_power_diff:.2f} mJ')
        time.sleep(1)
    except KeyboardInterrupt:
        break
cpu_power_after, mem_power_after = get_energy()
cpu_power_diff = cpu_power_after - cpu_power_before
mem_power_diff = mem_power_after - mem_power_before
print(f'Total CPU energy consumption: {cpu_power_diff:.2f} mJ')
print(f'Total memory energy consumption: {mem_power_diff:.2f} mJ')
